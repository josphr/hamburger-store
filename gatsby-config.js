module.exports = {
  siteMetadata: {
    title: `Hamburger Store`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
