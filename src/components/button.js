import React from 'react'
import styled from 'styled-components'

const ButtonStyled = styled.button`
    background-color : ${props => props.disabled ? '#AC9980' : '#8F5E1E' } ;
    color : ${props => props.disabled ? '#CCC' : 'white' } ;
    border: 1px solid ${props => props.disabled ? '#7E7365' : '#AA6817' } ;
    cursor: default;
    align-items: flex-start;
    text-align: center;
    display: block;
    font: inherit;
    padding: 5px;
    margin: 0 5px;
    width: 80px;
`

const Button = props => (
    <ButtonStyled
        disabled={props.disabled}
        onClick={props.click}
    >
        {props.children}
    </ButtonStyled>
)

export default Button