import React from 'react'
import Button from '../components/button'
import styled from 'styled-components'

const Background = styled.div`
    width: 100%;
    background-color: #CF8F2E;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-flow: column;
    flex-flow: column;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-shadow: 0 2px 1px #ccc;
    box-shadow: 0 2px 1px #ccc;
    margin:0 auto;
    padding: 10px 0;
`

const Label = styled.div`
    padding: 10px;
    font-weight: bold;
    width: 110px;
`
const BuildControl = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 5px 0;
`

const controls = [
    { Label: "Cheese", type: "cheese" },
    { Label: "Salad", type: "salad" },
    { Label: "Bacon", type: "bacon" },
    { Label: "Meat", type: "meat" },
]

const Builder = props => (
    <Background>
        <p>Current Price<strong> : {props.totalPrice.toFixed(2)} $ </strong></p>
        {controls.map(ctrl => 
        <BuildControl key={ctrl.type}>
            <Label>{ctrl.Label}</Label>
            <Button 
                click={() => props.removed(ctrl.type)}
            >
                Less
            </Button>
            <Button
                click={() => props.added(ctrl.type)}
            >
                More
            </Button>
        </BuildControl>)}
    </Background>
)

export default Builder