import React from 'react'
import Builder from '../components/builder'
import Link from 'gatsby-link'
import styled from 'styled-components'

const Cheese = styled.div`
    width: 90%;
    height: 4.5%;
    margin: 2% auto;
    background: -webkit-gradient(linear, left top, left bottom, from(#f4d004), to(#d6bb22));
    background: -webkit-linear-gradient(#f4d004, #d6bb22);
    background: -o-linear-gradient(#f4d004, #d6bb22);
    background: linear-gradient(#f4d004, #d6bb22);
    border-radius: 20px;
`

const Bacon = styled.div`
    width: 80%;
    height: 3%;
    background: -webkit-gradient(linear, left top, left bottom, from(#bf3813), to(#c45e38));
    background: -webkit-linear-gradient(#bf3813, #c45e38);
    background: -o-linear-gradient(#bf3813, #c45e38);
    background: linear-gradient(#bf3813, #c45e38);
    margin: 2% auto;
`

const Meat = styled.div`
    width: 80%;
    height: 8%;
    background: -webkit-gradient(linear, left top, left bottom, from(#7f3608), to(#702e05));
    background: -webkit-linear-gradient(#7f3608, #702e05);
    background: -o-linear-gradient(#7f3608, #702e05);
    background: linear-gradient(#7f3608, #702e05);
    margin: 2% auto;
    border-radius: 15px;
`

const Salad = styled.div`
    width: 85%;
    height: 7%;
    margin: 2% auto;
    background: -webkit-gradient(linear, left top, left bottom, from(#228c1d), to(#91ce50));
    background: -webkit-linear-gradient(#228c1d, #91ce50);
    background: -o-linear-gradient(#228c1d, #91ce50);
    background: linear-gradient(#228c1d, #91ce50);
    border-radius: 20px;
`

const Main = styled.div`
    margin:100px auto;
    width:450px;
    height:400px;
    text-align:center;
    overflow : auto;
    padding : 40px;
`

const BreadTop = styled.div`
    height:20%;
    width:88%;
    background:linear-gradient(#bc581e,#e27b36);
    border-radius: 50% 50% 0 0;
    margin:2% auto;
    box-shadow : inset -15px 0 #c15711;
    position:relative;
`

const BreadBottom = styled.div`
    height:13%;
    width:88%;
    background: linear-gradient(#F08E4A, #e27b36);
    border-radius: 0 0 30px 30px;
    margin:0 auto;
    box-shadow:inset -15px 0 #c15711;
`

const FirstSeed = styled.div`
    width:10%;
    height:15%;
    position:absolute;
    background-color:white;
    left:30%;
    top:50%;
    border-radius:40%;
    transform:rotate(-20deg);
    box-shadow:inset -2px -3px #c9c9c9;
    &:before {
      content: "";
      width: 100%;
      height: 100%;
      position: absolute;
      background-color: white;
      left: 180%;
      top: -50%;
      border-radius: 40%;
      -webkit-transform: rotate(60deg);
      -ms-transform: rotate(60deg);
      transform: rotate(60deg);
      -webkit-box-shadow: inset -1px -3px #c9c9c9;
      box-shadow: inset -1px -3px #c9c9c9;
    }
    &:after {
      content: "";
      width: 100%;
      height: 100%;
      position: absolute;
      background-color: white;
      left: -170%;
      top: -260%;
      border-radius: 40%;
      -webkit-transform: rotate(60deg);
      -ms-transform: rotate(60deg);
      transform: rotate(60deg);
      -webkit-box-shadow: inset -1px 2px #c9c9c9;
      box-shadow: inset -1px 2px #c9c9c9;
    }
`

const SecondSeed = styled.div`
    width: 10%;
    height: 15%;
    position: absolute;
    background-color: white;
    left: 64%;
    top: 50%;
    border-radius: 40%;
    -webkit-transform: rotate(10deg);
    -ms-transform: rotate(10deg);
    transform: rotate(10deg);
    -webkit-box-shadow: inset -3px 0 #c9c9c9;
    box-shadow: inset -3px 0 #c9c9c9;
    &:before {
      content: "";
      width: 100%;
      height: 100%;
      position: absolute;
      background-color: white;
      left: 150%;
      top: -130%;
      border-radius: 40%;
      -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
      transform: rotate(90deg);
      -webkit-box-shadow: inset 1px 3px #c9c9c9;
      box-shadow: inset 1px 3px #c9c9c9;
    }
`


const Text = styled.p`
    font-size:1.2rem;
    font-weight:bold;
    margin:1em 0;
`

const INGREDIENTS_PRICE = {
  salad: 0.5,
  bacon: 0.7,
  meat: 1.3,
  cheese: 0.4,
}

class IndexPage extends React.Component {
    
    constructor(props){
      super(props)
      this.state = {
        
        totalPrice : 4,
        ingredients : []
      }
    }

    addedIngredient = ingredient => {
        this.setState(prevState => ({
          totalPrice : prevState.totalPrice + INGREDIENTS_PRICE[ingredient],
          ingredients : [...prevState.ingredients,ingredient]
        }))
    }
    removedIngredient = ingredient => {
        this.setState({

        })
    }

    render(){
      const { ingredients } = this.state
      console.log(this.state)
      return(
        <div>
          <Main>
            <BreadTop>
            <FirstSeed/>
            <SecondSeed/>
            </BreadTop>
            {ingredients.length > 0 ? 
              ingredients.map((i,idx) => {
                switch(i){
                  case 'bacon' : return <Bacon key={idx} />
                  case 'salad' : return <Salad key={idx}/>
                  case 'cheese' : return <Cheese key={idx}/>
                  case 'meat' : return <Meat key={idx}/>
                }
              })
             : <Text>
              Start to add some ingredients
            </Text>
            }
            <BreadBottom/>
          </Main>
          <Builder
            added={this.addedIngredient}
            removed={this.removedIngredient}
            totalPrice={this.state.totalPrice}
          />
        </div>
      )
    }
}

export default IndexPage
